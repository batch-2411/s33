// 1 Fetch and get all the titles and console log the titles

const fetchTodos = async () => {
	let result = await fetch('https://jsonplaceholder.typicode.com/todos');
	let todos = await result.json();
	let todosTitle = todos.map(todo => todo.title);
	console.log(todosTitle);
}

fetchTodos();

// 2 Fetch a single todo and console log its title and status

const fetchATodo = async (id) => {
	let result = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`);
	let todo = await result.json();
	console.log(todo);
	// console.log('Title: ' + todo.title);
	// console.log('Completed: ' +todo.completed);
}

fetchATodo(1);

// 3 create a to do item using POST

const addTodo = async () => {
	let result = await fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			"userId": 1,
			"title": 'Created To Do List Item',
			"completed": false
		})
	})
	let data = await result.json();
	console.log(data);

}

addTodo();

// 4 update a to do item using PUT

const updateTodo = async (id) => {
	let result = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			"userId": 1,
			"title": "Updated To D List Item",
			"description": "To update the my to do list with a different data structure",
			"status": "Pending",
			"dateCompleted": "Pending"
		})
	})
	let data = await result.json();
	console.log(data);

}

updateTodo(1);

// 5 Create a fetch request to update  to do list using PATCH

const updateStatusTodo = async (id) => {
	let result = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			"status": "Complete",
			"dateCompleted": "01/31/23"
		})
	})
	let data = await result.json();
	console.log(data);

}

updateStatusTodo(1);

// Create a fetch request to delete an item

const deleteTodo = async (id) => {
	let result = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
		method: 'DELETE'
	})
	let data = await result.json();
	console.log(data);
}

deleteTodo(1);